/* 
 * File:   config.h
 * Author: PhilG
 *
 * Created on October 9, 2015, 4:06 PM
 */

/***** CONFIGURATION *****/
// ext reset off, code protect on, no watchdog, int RC clock 

//__CONFIG(MCLRE_OFF & CP_ON & WDTE_OFF & FOSC_INTOSCIO); 

#pragma config FOSC = INTOSC  // Oscillator Selection bits (INTOSCIO oscillator: I/O function on RA4/AN3/T1G/OSC2/CLKOUT, I/O function on RA5/T1CKI/OSC1/CLKIN)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled and can be enabled by SWDTEN bit of the WDTCON register)

#pragma config MCLRE = OFF      // MCLR Pin Function Select bit (MCLR pin is alternate function, MCLR function is internally disabled)
#pragma config CP = ON          // Code Protection bit (Program memory is external read and write protected)
#pragma config BOREN = ON       // Brown-out Reset Selection bits (BOR enabled)
#pragma config WRT = OFF        // Flash Program Memory Self Write Enable bits (Write protection off)
#pragma config CLKOUTEN = OFF
#pragma config LVP = OFF


