/* 
 * File:   GTS 1000 EL V2.h
 * Author: PhilG
 *
 * Created on February 5, 2019, 3:57 PM
 */

/***** GLOBAL VARIABLES *****/

volatile uint16_t		MasterCounter;
volatile uint8_t		MasterTimerFlag;
volatile uint16_t		TenthSecondsTimer;
volatile uint8_t		DimFlag;
volatile uint24_t		MasterTimerSetting;
volatile uint16_t		TenthSecondsTimerMax;
volatile uint8_t		CycleCounter;           //was uint16_t
volatile uint8_t		CyclesPerTenthSecond;   //was uint16_t
volatile uint16_t		MaxBrightness;
volatile uint8_t		OnOffState;
uint16_t				MinBrightness;
uint16_t                PWMDutyCycle;
uint8_t                 Blue;
uint8_t                 Red;
uint8_t                 CurrentColor;
uint8_t                 ModeChange;

//Pulsing
uint8_t                  PulseDimming;
uint16_t                 PulseDutyCycle;
uint8_t                  Pulsing;
uint16_t                 StepSize;
uint8_t                 Frequency;

uint8_t                 LEDTickCounter;
uint8_t                 TickCounterMax;
uint16_t                PulseMinBrightness;
uint16_t                PulsePeriod;
uint16_t                PeriodCounter;

uint8_t                 TempCalc;
uint16_t                PulseLimitBrightness;




extern void initPIC(void);
int UpdateBrightness(int PWMDutyCycle);
void CheckTimerSetting(void);
void EnableTimerZero(void);

