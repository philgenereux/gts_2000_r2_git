/* 
 * File:   defines.h
 * Author: PhilG
 *
 * Created on February 5, 2019, 4:21 PM
 */

#define _XTAL_FREQ              8000000     // oscillator frequency for _delay()
#define Button                  PORTAbits.RA3
#define TimerSwitchTestPin      RA1   //Pin turns on to energize circuit to test timer switch position
#define TimerStatePin1          RA4   //two pins used to detect timer switch position
#define TimerStatePin2          RA5
#define RED                     1
#define BLUE                    0
#define Timer1Length            59000

#define Timer0Frequency         1000;
//#define PulseFrequencyx10       10;
#define PulsePeriodInitialValue 80  //now variable period 80/160
#define PulsePeriodFinalValue   160
//#define PeriodCounterMax        250   //variable change how long it takes for the frequency to change
#define PeriodChange            1
#define CyclesPerTenthSecond    12	//Used to be 33 for 1 second. Prescalar reduced by 8, so 4 should be almost 1 second.
#define MinimumPulseBrightness  11
#define MinimumUserDimmingLevel 1400
#define PulseDepthPercentage    3   //Divide by this number
#define MaxBrightnessSetting    5000