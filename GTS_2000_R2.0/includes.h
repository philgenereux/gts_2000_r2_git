/* 
 * File:   includes.h
 * Author: PhilG
 *
 * Created on February 5, 2019, 3:58 PM
 */

#include <htc.h>
#include <stdint.h>
#include <xc.h>
#include <stdio.h>
#include "config.h"
#include "GTS 1000 EL V2.h"
#include "defines.h"
#include "pic12f1501_add.h"
#include "Software_UART.h"
