/************************************************************************
*                                                                       *
*   Filename:      GTS 2000 v1.0.c		                                *
*   Date:          2/05/19                                              *
*   File Version:  1.0                                                  *
*                                                                       *
*   Author:        Phil Genereux                                        *
*   Company:       Headwaters R&D                                       *
*                                                                       *
*************************************************************************
*                                                                       *
*   Architecture:  Midrange PIC                                         *
*   Processor:     12F1501 (Originally 12F615)                          *
*   Compiler:      XC8 (Lite mode)                                      *
*                                                                       *
*************************************************************************
*                                                                       *
*   Files required: pic12f1501.h                                        *
*                                                                       *
*************************************************************************
*                                                                       *
*   Description:    					                                *
*                                                                       *
*   GTS 2000 with 3 programs: Dimming with 3 timer settings. Initial 	*
*   brightness is selected by the user by holding the button down. 		*
*   All features have not been implemented yet                          *
*                                                                       *  
*   Version 2.0 adds the option to choose red or blue light color       *
*                                                                       *
*   PIC12F1501 replaces PIC12f615 which gives a second PWM channel      *
*************************************************************************
*                                                                       *
*   Pin assignments:                                                    *
*       RA0 = Blue LEDs                                                 *
*       RA2 = Red LEDs                                                  *
*       RA3 = Button                                                    *
************************************************************************/

#include "includes.h"





/***** MAIN PROGRAM *****/
void main()
{

    //PWM1 = RA2 = Blue (for testing)
    //PWM2 = RA0 = Red
    
    initPIC();

    
   // printf("Start\n\r");
   
	for (;;){
	
		if ((DimFlag) && (PWMDutyCycle > MinBrightness)){
			PWMDutyCycle--;
            if(!Pulsing){       //Update brightness if we are not pulsing]
			UpdateBrightness(PWMDutyCycle);
            }
			DimFlag = 0;

		}


		//If the master timer has expired, enter sleep mode
		if (MasterTimerFlag || OnOffState ){ //
			PWMDutyCycle = 0;
			OnOffState = 1;
//			PWMDutyCycle = MaxBrightness;
			UpdateBrightness(PWMDutyCycle);
            //printf("SLEEP \n\r");
            //TimerSwitchTestPin = 0;
			__delay_ms(500);
			SLEEP();
		}
         
	}
}


//INTERRUPTS***************************************************************************************************

void interrupt isr(void){

 
    if(PIR1bits.TMR1IF == 1){
        
        
        // Reset the start of the upcounter
        TMR1H = Timer1Length >> 8;
        TMR1L = Timer1Length & 0xFF;   
        
        
        CycleCounter++;
		
		if(CycleCounter == CyclesPerTenthSecond){
			TenthSecondsTimer++;
			MasterCounter++;
			CycleCounter = 0;
            Frequency++;

		}

		if (TenthSecondsTimer == TenthSecondsTimerMax){

			DimFlag = 1;  
			TenthSecondsTimer = 0;

            
		}	
        
	
		if (PWMDutyCycle == MinBrightness){
			MasterTimerFlag = 1;
			MasterCounter = 0;
		}
        
                     
        PIR1bits.TMR1IF = 0;    //Clear the Timer 2 interrupt flag
   
        
    }
    
   /*TIMER0      **********************************************************************************************/ 
	if (INTCONbits.TMR0IF == 1){

        
        
        if(Pulsing && !OnOffState){
            LEDTickCounter++;

            if(LEDTickCounter >= TickCounterMax){
                LEDTickCounter = 0;
                PeriodCounter++;
                
             
            if(PulsePeriod <= PulsePeriodFinalValue){    
                //PulsePeriod = 2*((PulsePeriodFinalValue - PulsePeriodInitialValue)*(MaxBrightness-PWMDutyCycle))/MaxBrightness + PulsePeriodInitialValue;
                TempCalc = ((PulsePeriodFinalValue/2 - PulsePeriodInitialValue/2)*(MaxBrightness/3-PWMDutyCycle/3))/MaxBrightness;
                PulsePeriod = 6*TempCalc + PulsePeriodInitialValue;
                
            }
                
            StepSize = PWMDutyCycle/PulsePeriod;    // 1000 steps / 100 Hz timer
            
            PulseLimitBrightness = MaxBrightness/5;
            
            if(PWMDutyCycle >= PulseLimitBrightness){
            
            if(PulseDimming){
                
                PulseDutyCycle -= StepSize;

                PulseMinBrightness = PWMDutyCycle/PulseDepthPercentage;
                
                if(PulseDutyCycle <= PulseMinBrightness){    //If we are at the minimum percentage of the linear brightness level
                    PulseDimming = 0;
                }

            }else{

                PulseDutyCycle += StepSize;

                if(PulseDutyCycle >= PWMDutyCycle){
                    PulseDimming = 1;

                    Frequency = 0;
                }

            }
            }else{
            
                PulseDutyCycle = PWMDutyCycle;
                //printf("Linear Dimming");

            }
           UpdateBrightness(PulseDutyCycle);

            }
        
        }
        TMR0 = 133;     //Sets the interval length of Timer0 interrupt (up counting to 255)

        INTCONbits.TMR0IF = 0; 	// Clear the TMR0 interrupt flag
	}

   /*BUTTON     **********************************************************************************************/     
    
	if (INTCONbits.IOCIF){		//Test to see if the I/O change flag was set
       
        __delay_ms(750);
	

        ModeChange = 1; //Short press so change mode
     
        if (Button == 0){	//Check if the user is holding the button down

			ModeChange = 0; //Don't change mode since the user was dimming the LEDs
        
			__delay_ms(100);

			while (Button == 0){		//Check if the user is holding the button down for a full 0.5 seconds and then dim for the duration
				MaxBrightness -= 5;
				__delay_ms(5);
				UpdateBrightness(MaxBrightness);
                //printf("Dimming\n\r");
				if (MaxBrightness == MinimumUserDimmingLevel){
					MaxBrightness = MaxBrightnessSetting;
				}
			}	
		}
        
    if(ModeChange && !OnOffState){
        
        if(!Pulsing){
            ModeChange = 0; //Clear Mode Change Flag so we don't change color
        
        
        PWM3DC = 0;        //Turn LEDs off for a quarter second
        PWM2DC = 0;
        PWM3LDCONbits.LDA = 1;
        PWM2LDCONbits.LDA = 1;
        //__delay_ms(250);
          
        }
        
        Pulsing ^= 1;   //Toggle pulse mode
    }    
        
    if(ModeChange){     //We want to change mode
        
        
                
        if(!OnOffState){    //IF UNIT IS ON DO THIS
             if(Blue){
               Blue = 0;
               Red = 1;
               CurrentColor = RED;

             }else{

               OnOffState ^= 1;	//Toggle the on/off state
               CurrentColor = BLUE;
             }

        }else{              //IF UNIT IS OFF DO THIS

             OnOffState ^= 1;	//Toggle the on/off state
             if(CurrentColor == RED){
                 Red = 1;
                 Blue = 0;
             }else{
               Red = 0;
               Blue = 1;
               CurrentColor = BLUE;
             }
        }
        
        ModeChange = 0;  //Reset the mode change flag 
    }

		//Reset the timer variables to restart
        
		CycleCounter = 0;
		TenthSecondsTimer = 0;
		MasterCounter = 0;
		DimFlag = 0;
		MasterTimerFlag = 0;
        
        
        PWMDutyCycle = MaxBrightness;
		if(!Pulsing){
            UpdateBrightness(MaxBrightness);
        }
        PulseDutyCycle = MaxBrightness;		
		CheckTimerSetting();	//Reset the brightness and the timer length every time the user presses the button
        PulsePeriod = PulsePeriodInitialValue;      //Reset pulse period when button is pressed
            
	__delay_ms(250);

	IOCAF = 0;  //Clear the I/O change flag
	} 

}

//***************************************************************************************************


UpdateBrightness(PWMDutyCycle){

    if(Blue){

        //Turn Red LEDs off

        PWM2DC = 0;
        PWM2LDCONbits.LDA = 1;    //Update the PWM values
        //Set the Blue LED Duty Cycle

        PWM3DC = PWMDutyCycle;
        PWM3LDCONbits.LDA = 1;       
    
    }
    
    if(Red){
        
        //Turn Blue LEDs off
        PWM3DC = 0;
        PWM3LDCONbits.LDA = 1;  

        //Set the Red LED Duty Cycle

        PWM2DC = PWMDutyCycle;
        PWM2LDCONbits.LDA = 1; 
    
    }
    
    if(PWMDutyCycle == 0){  //Turn off both LEDs
        PWM3DC = 0;
        PWM2DC = 0;        
    }


}


void CheckTimerSetting(void){

// disable Timer0 Interrupt
	T0IE = 0;

	TimerSwitchTestPin = 1;	//Turn on the switch test pin 
	__delay_ms(10);

	if (TimerStatePin2){
		MasterTimerSetting = 50000;  	// Left switch position = 6000 s = 10 min
       // printf("10 minutes \n\r");

	}
	if (TimerStatePin1){
		MasterTimerSetting = 100000;		// Middle switch position = 12000 s = 20 min
       // printf("20 minutes \n\r");

  	}
	if (TimerStatePin1 == 0 && TimerStatePin2 == 0){
		//MasterTimerSetting = 18450;     // Right switch position = 18000 s = 30 min
        MasterTimerSetting = 150000;    //TEST 40000 is ~8 min
       // printf("30 minutes \n\r");
	}

	TimerSwitchTestPin = 0;	//Turn off the switch test pin  
	__delay_ms(10);

	//Check position of timer switch and set the timer
	TenthSecondsTimerMax = MasterTimerSetting/MaxBrightness; // Calculate how often the LED must be dimmed (MaxBrightness is the number of steps to dim to zero)
    //with 5,000 steps (MaxBrightness) 5,000 ~= 1 minute
   // printf("TimerSecMax: ");
   // printf("%d\n\r",TenthSecondsTimerMax );
            
// enable and init Timer0 Interrupt
EnableTimerZero();        
}

void EnableTimerZero(void){

	T0IF = 0; 	// Clear the TMR0 interrupt flag
	TMR0 = 0;

	__delay_ms(10);
// enable Timer0 Interrupt
	T0IE = 1;
}

void putch(char data) {
    UART_Transmit(data);
}