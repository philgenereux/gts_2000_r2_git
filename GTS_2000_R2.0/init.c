/* 
 * File:   init.h
 * Author: PhilG
 *
 * Created on February 5, 2019, 3:34 PM
 */
#include "includes.h"



void initPIC(void){
    
    
    
    Blue = 1;   //Start with Blue color
    Red = 0;
    ModeChange = 0;
    


//	MaxBrightness = 0b00111111;
	MaxBrightness = MaxBrightnessSetting;		//Max out of 1024 (using old 10 bit resolution to make timers work)
	MinBrightness = 1;
	MasterCounter = 0;
	MasterTimerFlag = 0;
	MasterTimerSetting = 120;	//TO BE DEFINED BY POSITION OF 3 POSITION SLIDE SWITCH*************************************
	TenthSecondsTimer = 0;
	DimFlag = 0;
	CycleCounter = 0;
// #define now   CyclesPerTenthSecond = 24;	//Used to be 33 for 1 second. Prescalar reduced by 8, so 4 should be almost 1 second.
//    CyclesPerTenthSecond = 1;       //TEST
//	TenthSecondsTimerMax = 2;		//CALCULATED FROM MASTERTIMERSETTING AND INITIALBRIGHTNESS LEVEL
	OnOffState = 0;
    
    PulsePeriod = PulsePeriodInitialValue;      //Initial value
    PeriodCounter = 0;

    
    Frequency = 0;
    
    // Initialisation
    TRISA = 0b111000;          // configure GP0,1,2 (only) as an output

	ANSELA = 0;	//Configure all pins in digital mode


//Test program*************************************************


//**********************************************************
    APFCONbits.P2SEL = 0;   //PWM 2 on RA0
    APFCONbits.P1SEL = 1;
   
    PWM3CLKCONbits.CS = 0;  //FOSC clock source
    PWM3CLKCONbits.PS = 0b000;  //0 prescalar on clock
    
    //PWM1 replaced by PWM3
    
    PWM1CONbits.EN = 0;     //PWM1 off
    PWM1CONbits.POL = 0;    //Active high
 
    PWM3PR = (MaxBrightnessSetting + 5);  //1024 old value
    PWM3PH = 0; //Phase counter 0
    PWM3OFL = 1; // PWM2OFL 1; 
    PWM3INTE = 0; 
    PWM3OFCONbits.OFM = 0;  //Independent mode
    PWM3LDCONbits.LDT = 0;  //Load buffer trigger disabled
    
    PWM3CONbits.EN = 1;     //PWM3 on
    PWM3CONbits.OE = 1;     //PWM3 output enable on pin
    PWM3CONbits.OUT = 1;    //TEST ??
    PWM3CONbits.POL = 0;    //Active high
    PWM3CONbits.MODE = 0;    //Standard PWM
    
    PWM2PR = (MaxBrightnessSetting + 5);
    PWM2PH = 0; //Phase counter 0
    PWM2OFL = 1; // PWM2OFL 1; 
    PWM2INTE = 0; 
    PWM2OFCONbits.OFM = 0;  //Indepedent mode
    PWM2LDCONbits.LDT = 0;  //Load buffer trigger disabled
    
    PWM2CLKCONbits.CS = 0;  //FOSC clock source
    PWM2CLKCONbits.PS = 0b000;  //0 prescalar on clock
    
    PWM2CONbits.EN = 1;         //PWM1 on
    PWM2CONbits.OE = 1;     //PWM1 output enable on pin
    PWM2CONbits.OUT = 1;    //TEST ??
    PWM2CONbits.POL = 0;    //Active high
    PWM2CONbits.MODE = 0;    //Standard PWM 
    
    
    T2CONbits.TMR2ON = 1;
    T2CONbits.T2OUTPS = 2;  //1:3 postscalar
    T2CONbits.T2CKPS = 0;     //1 prescalar
//	CCPR1L 	= 0b00000011;
//	APFCON 	= 0b00010000;		//PWM Parameters

//  GPIO = 0b000111;	//Turn on both LEDS

    OSCCON = 0b01110010; //8 MHz Internal oscillator

// Configure Timer0
	OPTION_REGbits.TMR0CS = 0;	// Select Timer mode for Timer0
	OPTION_REGbits.PSA = 0;		// Assign prescalar to Timer0
	OPTION_REGbits.PS = 0b010;	// prescalar = 8 (8 times faster)


//	PWMDutyCycle = MAX_DUTY_CYCLE;		// Start with the duty cycle at the maximum value
	PWMDutyCycle = 0;
	UpdateBrightness(PWMDutyCycle);		//Update the PWM Level for the LEDs

	OnOffState = 1;		//Start with unit off

// Start program

	CheckTimerSetting();

	__delay_ms(100);

// Enable and init Timer0 Interrupt
    
    IOCANbits.IOCAN3 = 1;       //Enable interrupt on change on negative edge
    
    INTCONbits.GIE = 1;     //Global interrupts on
    INTCONbits.IOCIE = 1;   //Interrupt on change on
    INTCONbits.PEIE = 0;
	
    EnableTimerZero();    
	INTCONbits.IOCIF = 0;	//Clear the I/O change flag

    PIE1bits.TMR1IE = 1;        //Enable Timer 1 interrupt
    
    PIR1bits.TMR1IF = 0;    //Clear the Timer 1 interrupt flag
    
    
InitSoftUART();

//Setup Timer 1

T1CONbits.TMR1CS = 0b01;    //Source is system clock FOSC
T1CONbits.T1CKPS = 0b00;       //1:1 prescale
T1CONbits.nT1SYNC = 0;       //Sync with FOSC
T1CONbits.TMR1ON = 1;       //timer 1 on

T1GCON = 0;



TickCounterMax = 16;
Pulsing = 0;    //Start with solid color, no pulsing

    PWMDutyCycle = MaxBrightness;       //Make sure each mode matches in brightness
    PulseDutyCycle = PWMDutyCycle;	
    
TimerSwitchTestPin = 0;
    

    
}

